﻿////////////////////////////////////////////////////////////
// Bachelor of Software Engineering                       //
// Bachelor of Creative Technologies                      //
// Media Design School                                    //
// Auckland                                               //
// New Zealand                                            //
//--------------------------------------------------------//
// (c) 2020 Media Design School                           //
//========================================================//
//   File Name  : Box.cs                                  //
//--------------------------------------------------------//
//  Description : Checks collisions for the boxes         //
//                                                        //
//--------------------------------------------------------//
//    Author    : Brooklyn Dean BSE20021                  //
//    Author    : Toni Natta BSE20021                     //
//    Author    : Caleb Fepuleai BCT20021                 //
//    Author    : Waikuratautuhiorongo Papuni BCT20021    //
//--------------------------------------------------------//
//    E-mail    : brooklyn.dean@mds.ac.nz                 //
//    E-mail    : toni.natta@mds.ac.nz                    //
//    E-mail    : caleb.fepuleai@mds.ac.nz                //
//    E-mail    : waikuratautuhiorongo.papuni@mds.ac.nz   //
//                                                        //
////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class box : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
 
    }

    IEnumerator OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Bullet")
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            yield return new WaitForSeconds(5);
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        }

        if (collision.gameObject.tag == "Button")
        {
            Destroy(GameObject.FindWithTag("Door"));
        }
        if (collision.gameObject.tag == "Button2")
        {
            Destroy(GameObject.FindWithTag("Door2"));
        }
        if (collision.gameObject.tag == "Button3")
        {
            Destroy(GameObject.FindWithTag("Door3"));
        }
        if (collision.gameObject.tag == "Button4")
        {
            Destroy(GameObject.FindWithTag("Door4"));
        }
    }
}
