﻿////////////////////////////////////////////////////////////
// Bachelor of Software Engineering                       //
// Bachelor of Creative Technologies                      //
// Media Design School                                    //
// Auckland                                               //
// New Zealand                                            //
//--------------------------------------------------------//
// (c) 2020 Media Design School                           //
//========================================================//
//   File Name  : gravitySwapper.cs                       //
//--------------------------------------------------------//
//  Description : Code to swap the gravity                //
//                                                        //
//--------------------------------------------------------//
//    Author    : Brooklyn Dean BSE20021                  //
//    Author    : Toni Natta BSE20021                     //
//    Author    : Caleb Fepuleai BCT20021                 //
//    Author    : Waikuratautuhiorongo Papuni BCT20021    //
//--------------------------------------------------------//
//    E-mail    : brooklyn.dean@mds.ac.nz                 //
//    E-mail    : toni.natta@mds.ac.nz                    //
//    E-mail    : caleb.fepuleai@mds.ac.nz                //
//    E-mail    : waikuratautuhiorongo.papuni@mds.ac.nz   //
//                                                        //
////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySwapper : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            GetComponent<Rigidbody2D>().gravityScale *= -1;
        }
    }
}
