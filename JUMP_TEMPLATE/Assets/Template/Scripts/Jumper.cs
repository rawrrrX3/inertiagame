﻿////////////////////////////////////////////////////////////
// Bachelor of Software Engineering                       //
// Bachelor of Creative Technologies                      //
// Media Design School                                    //
// Auckland                                               //
// New Zealand                                            //
//--------------------------------------------------------//
// (c) 2020 Media Design School                           //
//========================================================//
//   File Name  : Jumper.cs                               //
//--------------------------------------------------------//
//  Description : Code for the player                     //
//                                                        //
//--------------------------------------------------------//
//    Author    : Brooklyn Dean BSE20021                  //
//    Author    : Toni Natta BSE20021                     //
//    Author    : Caleb Fepuleai BCT20021                 //
//    Author    : Waikuratautuhiorongo Papuni BCT20021    //
//--------------------------------------------------------//
//    E-mail    : brooklyn.dean@mds.ac.nz                 //
//    E-mail    : toni.natta@mds.ac.nz                    //
//    E-mail    : caleb.fepuleai@mds.ac.nz                //
//    E-mail    : waikuratautuhiorongo.papuni@mds.ac.nz   //
//                                                        //
////////////////////////////////////////////////////////////

// These are some generally expected includes that Unity will pre-add for you, generally no need to touch them
using System.Collections;
using System.Collections.Generic;
// This is the most important include (as it gives us access to the UnityEngine structures)
using UnityEngine;

// CLASS 
// Jumper (MonoBehaviour) - inheriting from MonoBehaviour to ensure we can operate within Unity
// DESCRIPTION 
// Has simplistic functionality to allow the player to "jump" towards the mouse cursor
// Can be extended in a variety of ways to enable more interesting behaviours
public class Jumper : MonoBehaviour
{

    //float horizontalMove = 12.0f;
    public Animator animator;
    // This is an inspector-level element that creates a header in our script inspector in Unity
    // Use these to separate out big blocks of inspector values
    [Header("Jumping")]

    // The tooltip applies to the next inspectable value and provides information when it's hovered in the inspector in Unity
    [Tooltip("The speed that we travel when we jump")] 
    // This is how fast we will travel when we jump in the scene (turn it up to jump faster, down to jump slower)
    public float jumpStrength = 12.0f;
    public float speed = 0.0f;

    private bool doubleJump = false;
    private bool onGround = true;

    public float cooldownLength = 2.0f;
    public float cooldownTimer = 0.0f;

    public bool checkpoint1 = false;
    public bool checkpoint2 = false;
    public bool checkpoint3 = false;
    public bool checkpoint4 = false;
    public bool checkpoint5 = false;

    public UIManager uiManager;

    [SerializeField] private AudioClip jumpClip;
    [SerializeField] private AudioSource jumping;

    [SerializeField] private AudioClip doubleJumpClip;
    [SerializeField] private AudioSource doubleJumping;

    // The Update function is called every render frame by Unity to handle object processing (can be public/private/protected/etc.)
    private void Update()
    {
        //// Read the mouse position from input
        //Vector3 mousePos = Input.mousePosition;
        //// Zero out our mouse's z as a safety measure
        //mousePos.z = 0.0f;

        //// Use the camera to transform our screen position to a world position
        //Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
        //// Set our target z position to our existing z position so that we ensure that we only have difference in x and y
        //targetPos.z = transform.position.z;

        //// Get the vector between where our mouse is and where we are
        //// Then normalize it (magnitude = 1) so that we don't consider how close/far away the mouse is, just what direction
        //Vector3 targetDir = (targetPos - transform.position).normalized;

        //// If we press the left mouse button (only triggered first frame of pressing)
        //if (Input.GetMouseButtonDown(0) && cooldownTimer == 0.0f)
        //{
        //    // Get our Rigidbody2D and set its velocity to towards the mouse at the strength of our jump
        //    GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * jumpStrength;
        //    cooldownTimer = cooldownLength;
        //}
        animator.SetFloat("speed", Mathf.Abs(speed));

        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
            animator.Play("jumper_left");
        }

        else if (Input.GetKey(KeyCode.D)) 
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
            animator.Play("jumper_moving");
        } 

        else if (Input.GetKey(KeyCode.E)) 
        {
            animator.Play("jumper_jump");
        } 

        else if (Input.GetKey(KeyCode.Mouse0))
        {
            animator.Play("jumper_glue");
        }

        else if (!onGround)
        {
            animator.Play("jumper_jump");
        }

        else 
        {
            animator.Play("jumper_idle");
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (onGround)
            {
                GetComponent<Rigidbody2D>().AddForce(transform.TransformDirection(Vector3.up) * 1600);
                onGround = false;
                doubleJump = true;

                jumping.PlayOneShot(jumpClip, 0.7F);
            }
            else
            {
                if (doubleJump)
                {
                    doubleJump = false;
                    GetComponent<Rigidbody2D>().AddForce(transform.TransformDirection(Vector3.up) * 2200);

                    doubleJumping.PlayOneShot(doubleJumpClip, 0.7F);
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onGround = true;
        }

        if (collision.gameObject.tag == "Vent")
        {
            uiManager.DisplayGameOver();
            Debug.Log("Collision");
        }

        if (collision.gameObject.tag == "Spikes")
        {
            uiManager.DisplayGameLose();
            Debug.Log("Collision");

            if (checkpoint1 == true)
            {
                transform.position = new Vector3(80, 7, -1);
            }
            else if (checkpoint2 == true)
            {
                transform.position = new Vector3(103, 7, -1);
            }
            else if (checkpoint3 == true)
            {
                transform.position = new Vector3(136, (float)-0.75, -1);
            }
            else if (checkpoint4 == true)
            {
                transform.position = new Vector3(140, 16, -1);
            }
            else if (checkpoint5 == true)
            {
                transform.position = new Vector3(146, 6, -1);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Checkpoint1")
        {
            checkpoint1 = true;
            Destroy(GameObject.FindWithTag("Checkpoint1"));
        }

        if (collider.tag == "Checkpoint2")
        {
            checkpoint1 = false;
            checkpoint2 = true;
            Destroy(GameObject.FindWithTag("Checkpoint2"));
        }

        if (collider.tag == "Checkpoint3")
        {
            checkpoint2 = false;
            checkpoint3 = true;
            Destroy(GameObject.FindWithTag("Checkpoint3"));
        }

        if (collider.tag == "Checkpoint4")
        {
            checkpoint3 = false;
            checkpoint4 = true;
            Destroy(GameObject.FindWithTag("Checkpoint4"));
        }

        if (collider.tag == "Checkpoint5")
        {
            checkpoint4 = false;
            checkpoint5 = true;
            Destroy(GameObject.FindWithTag("Checkpoint5"));
        }
    }


}
