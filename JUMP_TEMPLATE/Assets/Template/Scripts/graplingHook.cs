﻿////////////////////////////////////////////////////////////
// Bachelor of Software Engineering                       //
// Bachelor of Creative Technologies                      //
// Media Design School                                    //
// Auckland                                               //
// New Zealand                                            //
//--------------------------------------------------------//
// (c) 2020 Media Design School                           //
//========================================================//
//   File Name  : graplingHook.cs                         //
//--------------------------------------------------------//
//  Description : Has the code for the grappling hook     //
//                                                        //
//--------------------------------------------------------//
//    Author    : Brooklyn Dean BSE20021                  //
//    Author    : Toni Natta BSE20021                     //
//    Author    : Caleb Fepuleai BCT20021                 //
//    Author    : Waikuratautuhiorongo Papuni BCT20021    //
//--------------------------------------------------------//
//    E-mail    : brooklyn.dean@mds.ac.nz                 //
//    E-mail    : toni.natta@mds.ac.nz                    //
//    E-mail    : caleb.fepuleai@mds.ac.nz                //
//    E-mail    : waikuratautuhiorongo.papuni@mds.ac.nz   //
//                                                        //
////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Build.Reporting;
using UnityEngine;

public class graplingHook : MonoBehaviour
{
    public LineRenderer grapplingLine;
    DistanceJoint2D distanceJoint;

    Vector3 targetPosition;
    RaycastHit2D hitObj;

    public float maxDistance = 10.0f;
    public LayerMask collisionMask;
    public float step = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        distanceJoint = GetComponent<DistanceJoint2D>();
        distanceJoint.enabled = false;
        grapplingLine.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        // If player is not touching the object make the rope shorter 
        if (distanceJoint.distance > 0.0f)
        {
            distanceJoint.distance -= step;
        }
        else
        {
            grapplingLine.enabled = false;
            distanceJoint.enabled = false;
        }
        
        // While player presses E
        if (Input.GetKeyDown(KeyCode.E))
        { 
            // Get mouse position
            targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPosition.z = 0;

            hitObj = Physics2D.Raycast(transform.position, targetPosition - transform.position, maxDistance, collisionMask);

            //Check if collision with an object with a rigidbody 
            if (hitObj.collider != null && hitObj.collider.gameObject.GetComponent<Rigidbody2D>() != null && hitObj.transform.tag != "Wall" && hitObj.transform.tag != "Floor")
            {
                // Enable the joint
                distanceJoint.enabled = true;

                // Set the connected body to the rigitbody of the object we attached to
                distanceJoint.connectedBody = hitObj.collider.gameObject.GetComponent < Rigidbody2D >();

                // Set the anchor of the joint 
                distanceJoint.connectedAnchor = hitObj.point - new Vector2(hitObj.collider.transform.position.x, hitObj.collider.transform.position.y);

                // Set the distance between the points
                distanceJoint.distance = Vector2.Distance(transform.position, hitObj.point);

                // Enable the line and set its start & send position
                grapplingLine.enabled = true;
                grapplingLine.SetPosition(0, transform.position);
                grapplingLine.SetPosition(1, hitObj.point);
            }
        }

        // If E is being held update the line's start position as the player moves
        if (Input.GetKey(KeyCode.E))
        {
            grapplingLine.SetPosition(0, transform.position);
            grapplingLine.SetPosition(1, hitObj.collider.gameObject.transform.position);
        }

        // Once E is released disbale the line and the joint
        if(Input.GetKeyUp(KeyCode.E))
        {
            distanceJoint.enabled = false;
            grapplingLine.enabled = false;
        }
    }
}
