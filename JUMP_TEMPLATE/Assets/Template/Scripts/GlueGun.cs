﻿////////////////////////////////////////////////////////////
// Bachelor of Software Engineering                       //
// Bachelor of Creative Technologies                      //
// Media Design School                                    //
// Auckland                                               //
// New Zealand                                            //
//--------------------------------------------------------//
// (c) 2020 Media Design School                           //
//========================================================//
//   File Name  : GlueGun.cs                              //
//--------------------------------------------------------//
//  Description : Code for the glue gun                   //
//                                                        //
//--------------------------------------------------------//
//    Author    : Brooklyn Dean BSE20021                  //
//    Author    : Toni Natta BSE20021                     //
//    Author    : Caleb Fepuleai BCT20021                 //
//    Author    : Waikuratautuhiorongo Papuni BCT20021    //
//--------------------------------------------------------//
//    E-mail    : brooklyn.dean@mds.ac.nz                 //
//    E-mail    : toni.natta@mds.ac.nz                    //
//    E-mail    : caleb.fepuleai@mds.ac.nz                //
//    E-mail    : waikuratautuhiorongo.papuni@mds.ac.nz   //
//                                                        //
////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueGun : MonoBehaviour
{
    public GameObject projectile;
    public float force;

    [SerializeField] private AudioClip bulletClip;
    [SerializeField] private AudioSource bulletShoot;

    // Start is called before the first frame update
    void Start()
    {
        bulletShoot = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Read the mouse position from input
        Vector3 mousePos = Input.mousePosition;
        // Zero out our mouse's z as a safety measure
        mousePos.z = 0.0f;

        // Use the camera to transform our screen position to a world position
        Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
        // Set our target z position to our existing z position so that we ensure that we only have difference in x and y
        targetPos.z = transform.position.z;

        Vector3 targetDir = (targetPos - transform.position).normalized;

        if (Input.GetMouseButtonDown(0))
        {
            GameObject bullet = GameObject.Instantiate(projectile, transform.position, transform.rotation);
            //bullet.GetComponent<Rigidbody2D>().AddForce(transform.forward * force);
            bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * force;

            bulletShoot.PlayOneShot(bulletClip, 0.7F);

        }
    }
}
