﻿////////////////////////////////////////////////////////////
// Bachelor of Software Engineering                       //
// Bachelor of Creative Technologies                      //
// Media Design School                                    //
// Auckland                                               //
// New Zealand                                            //
//--------------------------------------------------------//
// (c) 2020 Media Design School                           //
//========================================================//
//   File Name  : UIManager.cs                            //
//--------------------------------------------------------//
//  Description : Shows and hides UI                      //
//                                                        //
//--------------------------------------------------------//
//    Author    : Brooklyn Dean BSE20021                  //
//    Author    : Toni Natta BSE20021                     //
//    Author    : Caleb Fepuleai BCT20021                 //
//    Author    : Waikuratautuhiorongo Papuni BCT20021    //
//--------------------------------------------------------//
//    E-mail    : brooklyn.dean@mds.ac.nz                 //
//    E-mail    : toni.natta@mds.ac.nz                    //
//    E-mail    : caleb.fepuleai@mds.ac.nz                //
//    E-mail    : waikuratautuhiorongo.papuni@mds.ac.nz   //
//                                                        //
////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Button playBtn, quitBtn, mainMenuBtn, GOquitBtn, retryBtn, GLquitBtn;

    public GameObject mainMenuUI, gameOverUI, gameLoseUI;

    private void Awake()
    {
        DisplayMainMenu();
        Time.timeScale = 0f;
    }

    // Start is called before the first frame update
    public void Start()
    {
        playBtn.onClick.AddListener(delegate { HideMainMenu(); Time.timeScale = 1f; });
        quitBtn.onClick.AddListener(delegate { Application.Quit(); });

        mainMenuBtn.onClick.AddListener(delegate { HideGameOver(); DisplayMainMenu(); });
        GOquitBtn.onClick.AddListener(delegate { Application.Quit(); });

        retryBtn.onClick.AddListener(delegate { HideGameLose(); });
        GLquitBtn.onClick.AddListener(delegate { Application.Quit(); });
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisplayMainMenu()
    {
        mainMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void HideMainMenu()
    {
        mainMenuUI.SetActive(false);
    }

    public void DisplayGameOver()
    {
        gameOverUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void HideGameOver()
    {
        gameOverUI.SetActive(false);
    }

    public void DisplayGameLose()
    {
        gameLoseUI.SetActive(true);
    }

    public void HideGameLose()
    {
        gameLoseUI.SetActive(false);
    }
}
